// This config is only used by dependency cruiser
// The vue client generates its own config
module.exports = {
  resolve: {
    alias: {
      '@': './src',
    },
  },
};
