

### Local corpus

Local pieces are to be put, each one in its own directory, 
in `public/local-corpus` (do not put further directories or files).

You can look at the directory hierarchy of the pieces already there. Each piece hierachy is like that:

```
./public/local-corpus/piece-id/
  |-- info.json (metadata and path to sources files [1])
  |-- piece-id.mm.json (measure map file [2]) - optional
  |-- analyses/ -> you can put you annotation files (.dez [3]) here
  |-- sources/
       |-- audios/ [optional]
       |    |-- audio-1/
       |    |    |-- synchro.json
       |    |    |-- images
       |    |         |-- image-1/
       |    |         |    |-- image-1.[svg|png|jpg]
       |    |         |    |-- positions.json (onset-date)
       |    |         |-- ...
       |    |         |-- image-n/
       |    |              |-- same as image-1
       |    |-- ...
       |    |-- audio-n/
       |         |-- same ad audio-1
       |-- images/
            |-- image-1/
            |    |-- image-1.svg
            |    |-- positions.json (onset-x)
            |-- ...
            |-- image-n/
                 |-- same as image-1
```
[1] [info files format](https://gitlab.com/algomus.fr/dezrann/dezrann/-/blob/dev/docs/api/infos-json.md)
[2] [pyMeasureMap](https://github.com/measure-map/pyMeasureMap)
[3] [dez files format](https://gitlab.com/algomus.fr/dezrann/dezrann/-/blob/dev/docs/api/dez-format.md)

