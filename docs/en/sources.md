
# Sources and Canvas

"Linear" Score (--line-breaks=None)
  - (Dummy) pages/rows are created after parsing them
  - ==> CanvasSourceScore

3DScore
  - Pages/rows are given
  - ==> CanvasSource3D

Wave
  - ==> CanvasSourceScore

Sync
  - Synchronization is currently done only on Wave and (Linear) Score
  - ==> CanvasSourceScore


## Pour résumer

- CanvasSourceBase: wrapper permettant de choisir quel composant va gérer l'affichage de la source en se basant sur source.type
- CanvasSourceScore: composant gérant l'affichage des score et des wave
- CanvasSource3D: composant gérant l'affichage des 3d


