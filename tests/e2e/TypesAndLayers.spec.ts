import { test, expect } from '@playwright/test';

test.describe('Types And Layers', () => {
  test.beforeEach(async ({ page }) => {
    await page.goto('/~~/bwv847');
    await page.waitForSelector('.label-rect');
  });

  test('Check types and layers filter', async ({ page }) => {
    const labels = await page.locator('.label-rect').all();
    await expect(labels.length).toEqual(60);

    const settingsBtn = await page.locator('.v-icon.notranslate.v-icon--left.mdi.mdi-filter.theme--light').first();
    await settingsBtn.click();

    const typesAndLayersSelector = '.v-input.settings-bar-checkbox.v-input--hide-details.v-input--is-label-active.v-input--is-dirty.theme--light.v-input--selection-controls.v-input--checkbox.primary--text';
    await page.waitForSelector(typesAndLayersSelector);

    const typesAndLayers = await page.locator(typesAndLayersSelector).all();
    await expect(typesAndLayers.length).toEqual(11);
  });

  test('Toggle all filters and layers', async ({ page }) => {
    let labels = await page.locator('.label-rect').all();
    await expect(labels.length).toEqual(60);

    const settingsBtn = await page.locator('.v-icon.notranslate.v-icon--left.mdi.mdi-filter.theme--light').first();
    await settingsBtn.click();

    const typesAndLayersSelector = '.v-input.settings-bar-checkbox';
    await page.waitForSelector(typesAndLayersSelector);

    const typesAndLayers = await page.locator(typesAndLayersSelector).all();

    await typesAndLayers.at(0)?.click();
    labels = await page.locator('.label-rect').all();
    await expect(labels.length).toEqual(0);

    await typesAndLayers.at(0)?.click();
    labels = await page.locator('.label-rect').all();
    await expect(labels.length).toEqual(60);

    await typesAndLayers.at(8)?.click();
    labels = await page.locator('.label-rect').all();
    await expect(labels.length).toEqual(0);

    await typesAndLayers.at(8)?.click();
    labels = await page.locator('.label-rect').all();
    await expect(labels.length).toEqual(60);
  });

  test('Toggle specific filters and layers', async ({ page }) => {
    let labels = await page.locator('.label-rect').all();
    await expect(labels.length).toEqual(60);

    const settingsBtn = await page.locator('.v-icon.notranslate.v-icon--left.mdi.mdi-filter.theme--light').first();
    await settingsBtn.click();

    const typesAndLayersSelector = '.v-input.settings-bar-checkbox';
    await page.waitForSelector(typesAndLayersSelector);

    const typesAndLayers = await page.locator(typesAndLayersSelector).all();

    await typesAndLayers.at(1)?.click();
    labels = await page.locator('.label-rect').all();
    await expect(labels.length).toEqual(44);

    await typesAndLayers.at(1)?.click();
    labels = await page.locator('.label-rect').all();
    await expect(labels.length).toEqual(60);

    await typesAndLayers.at(9)?.click();
    labels = await page.locator('.label-rect').all();
    await expect(labels.length).toEqual(2);

    await typesAndLayers.at(9)?.click();
    labels = await page.locator('.label-rect').all();
    await expect(labels.length).toEqual(60);
  });
});
