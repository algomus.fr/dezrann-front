import BPlayerNotifier from '@/components/behaviors/player/BPlayerNotifier.vue';
import type { Duration, Label, Onset } from '@/data';
import type { LabelTime } from '@/data/Label';
import { initBehaviorContext } from './utils';

describe('BPlayerNotifier.vue', () => {
  const sortedLabels: LabelTime[] = [
    {
      onset: 0 as Onset, duration: 2 as Duration, playedIn: false, playedOut: false,
    },
    {
      onset: 1 as Onset, duration: 12 as Duration, playedIn: false, playedOut: false,
    },
    {
      onset: 2 as Onset, duration: 7 as Duration, playedIn: false, playedOut: false,
    },
    {
      onset: 7 as Onset, duration: 5 as Duration, playedIn: false, playedOut: false,
    },
  ];

  it('checks that initiateQueues fills localLabels and empties runningLabels', () => {
    const { behavior, actions } = initBehaviorContext(BPlayerNotifier, {
      musicalTime: 0,
      playing: false,
    });
    actions.getSortedLabels = jest.fn(() => sortedLabels as unknown as Label[]);

    behavior.setData({ runningLabels: sortedLabels });
    expect(behavior.vm.$data.localLabels.length).toEqual(0);
    expect(behavior.vm.$data.runningLabels.length).toEqual(4);

    behavior.vm.initiateQueues();
    expect(behavior.vm.$data.localLabels.length).toEqual(4);
    expect(behavior.vm.$data.runningLabels.length).toEqual(0);
  });

  it('checks that handlePassedTimes inserts correctly [musicalTime=0]', () => {
    const { behavior } = initBehaviorContext(BPlayerNotifier, {
      musicalTime: 0,
      playing: false,
    });

    behavior.setData({ localLabels: [...sortedLabels] });
    behavior.vm.handlePassedTimes();

    expect(behavior.vm.$data.localLabels.length).toEqual(3);
    expect(behavior.vm.$data.runningLabels.length).toEqual(1);

    expect({ ...behavior.vm.$data.localLabels[0], playedIn: false, playedOut: false })
      .toEqual(sortedLabels[1]);
    expect({ ...behavior.vm.$data.localLabels[1], playedIn: false, playedOut: false })
      .toEqual(sortedLabels[2]);
    expect({ ...behavior.vm.$data.localLabels[2], playedIn: false, playedOut: false })
      .toEqual(sortedLabels[3]);
    expect({ ...behavior.vm.$data.runningLabels[0], playedIn: false, playedOut: false })
      .toEqual(sortedLabels[0]);
  });

  it('checks that handlePassedTimes inserts correctly [musicalTime=1]', () => {
    const { behavior } = initBehaviorContext(BPlayerNotifier, {
      musicalTime: 1,
      playing: false,
    });

    behavior.setData({ localLabels: [...sortedLabels] });
    behavior.vm.handlePassedTimes();

    expect(behavior.vm.$data.localLabels.length).toEqual(2);
    expect(behavior.vm.$data.runningLabels.length).toEqual(2);

    expect({ ...behavior.vm.$data.localLabels[0], playedIn: false, playedOut: false })
      .toEqual(sortedLabels[2]);
    expect({ ...behavior.vm.$data.localLabels[1], playedIn: false, playedOut: false })
      .toEqual(sortedLabels[3]);
    expect({ ...behavior.vm.$data.runningLabels[0], playedIn: false, playedOut: false })
      .toEqual(sortedLabels[0]);
    expect({ ...behavior.vm.$data.runningLabels[1], playedIn: false, playedOut: false })
      .toEqual(sortedLabels[1]);
  });

  it('checks that handlePassedTimes inserts correctly [musicalTime=2]', () => {
    const { behavior } = initBehaviorContext(BPlayerNotifier, {
      musicalTime: 2,
      playing: false,
    });

    behavior.setData({ localLabels: [...sortedLabels] });
    behavior.vm.handlePassedTimes();

    expect(behavior.vm.$data.localLabels.length).toEqual(1);
    expect(behavior.vm.$data.runningLabels.length).toEqual(2);

    expect({ ...behavior.vm.$data.localLabels[0], playedIn: false, playedOut: false })
      .toEqual(sortedLabels[3]);
    expect({ ...behavior.vm.$data.runningLabels[0], playedIn: false, playedOut: false })
      .toEqual(sortedLabels[2]);
    expect({ ...behavior.vm.$data.runningLabels[1], playedIn: false, playedOut: false })
      .toEqual(sortedLabels[1]);
  });

  it('checks that handlePassedTimes inserts correctly [musicalTime=7]', () => {
    const { behavior } = initBehaviorContext(BPlayerNotifier, {
      musicalTime: 7,
      playing: false,
    });

    behavior.setData({ localLabels: [...sortedLabels] });
    behavior.vm.handlePassedTimes();

    expect(behavior.vm.$data.localLabels.length).toEqual(0);
    expect(behavior.vm.$data.runningLabels.length).toEqual(3);

    expect({ ...behavior.vm.$data.runningLabels[0], playedIn: false, playedOut: false })
      .toEqual(sortedLabels[2]);
    expect({ ...behavior.vm.$data.runningLabels[1], playedIn: false, playedOut: false })
      .toEqual(sortedLabels[3]);
    expect({ ...behavior.vm.$data.runningLabels[2], playedIn: false, playedOut: false })
      .toEqual(sortedLabels[1]);
  });

  it('checks that handlePassedTimes inserts correctly [musicalTime=12]', () => {
    const { behavior } = initBehaviorContext(BPlayerNotifier, {
      musicalTime: 12,
      playing: false,
    });

    behavior.setData({ localLabels: [...sortedLabels] });
    behavior.vm.handlePassedTimes();

    expect(behavior.vm.$data.localLabels.length).toEqual(0);
    expect(behavior.vm.$data.runningLabels.length).toEqual(1);

    expect({ ...behavior.vm.$data.runningLabels[0], playedIn: false, playedOut: false })
      .toEqual(sortedLabels[1]);
  });

  it('checks that handlePassedTimes inserts correctly [musicalTime=100]', () => {
    const { behavior } = initBehaviorContext(BPlayerNotifier, {
      musicalTime: 100,
      playing: false,
    });

    behavior.setData({ localLabels: [...sortedLabels] });
    behavior.vm.handlePassedTimes();

    expect(behavior.vm.$data.localLabels.length).toEqual(0);
    expect(behavior.vm.$data.runningLabels.length).toEqual(0);
  });

  it('checks that handlePassedTimes inserts correctly with multiple time changes', () => {
    const { behavior } = initBehaviorContext(BPlayerNotifier, {
      musicalTime: 100,
      playing: false,
    });

    behavior.setData({ localLabels: [...sortedLabels] });
    behavior.setProps({ musicalTime: 0 });
    behavior.vm.$nextTick(() => {
      behavior.vm.handlePassedTimes();

      expect(behavior.vm.$data.localLabels.length).toEqual(3);
      expect(behavior.vm.$data.runningLabels.length).toEqual(1);

      expect({ ...behavior.vm.$data.localLabels[0], playedIn: false, playedOut: false })
        .toEqual(sortedLabels[1]);
      expect({ ...behavior.vm.$data.localLabels[1], playedIn: false, playedOut: false })
        .toEqual(sortedLabels[2]);
      expect({ ...behavior.vm.$data.localLabels[2], playedIn: false, playedOut: false })
        .toEqual(sortedLabels[3]);
      expect({ ...behavior.vm.$data.runningLabels[0], playedIn: false, playedOut: false })
        .toEqual(sortedLabels[0]);

      behavior.setData({ localLabels: [...sortedLabels] });
      behavior.setProps({ musicalTime: 1 });
      behavior.vm.$nextTick(() => {
        behavior.vm.handlePassedTimes();

        expect(behavior.vm.$data.localLabels.length).toEqual(2);
        expect(behavior.vm.$data.runningLabels.length).toEqual(2);

        expect({ ...behavior.vm.$data.localLabels[0], playedIn: false, playedOut: false })
          .toEqual(sortedLabels[2]);
        expect({ ...behavior.vm.$data.localLabels[1], playedIn: false, playedOut: false })
          .toEqual(sortedLabels[3]);
        expect({ ...behavior.vm.$data.runningLabels[0], playedIn: false, playedOut: false })
          .toEqual(sortedLabels[0]);
        expect({ ...behavior.vm.$data.runningLabels[1], playedIn: false, playedOut: false })
          .toEqual(sortedLabels[1]);
        behavior.setData({ localLabels: [...sortedLabels] });
        behavior.setProps({ musicalTime: 2 });
        behavior.vm.$nextTick(() => {
          behavior.vm.handlePassedTimes();

          expect(behavior.vm.$data.localLabels.length).toEqual(1);
          expect(behavior.vm.$data.runningLabels.length).toEqual(2);

          expect({ ...behavior.vm.$data.localLabels[0], playedIn: false, playedOut: false })
            .toEqual(sortedLabels[3]);
          expect({ ...behavior.vm.$data.runningLabels[0], playedIn: false, playedOut: false })
            .toEqual(sortedLabels[2]);
          expect({ ...behavior.vm.$data.runningLabels[1], playedIn: false, playedOut: false })
            .toEqual(sortedLabels[1]);

          behavior.setData({ localLabels: [...sortedLabels] });
          behavior.setProps({ musicalTime: 7 });
          behavior.vm.$nextTick(() => {
            behavior.vm.handlePassedTimes();

            expect(behavior.vm.$data.localLabels.length).toEqual(0);
            expect(behavior.vm.$data.runningLabels.length).toEqual(3);

            expect({ ...behavior.vm.$data.runningLabels[0], playedIn: false, playedOut: false })
              .toEqual(sortedLabels[2]);
            expect({ ...behavior.vm.$data.runningLabels[1], playedIn: false, playedOut: false })
              .toEqual(sortedLabels[3]);
            expect({ ...behavior.vm.$data.runningLabels[2], playedIn: false, playedOut: false })
              .toEqual(sortedLabels[1]);

            behavior.setData({ localLabels: [...sortedLabels] });
            behavior.setProps({ musicalTime: 12 });
            behavior.vm.$nextTick(() => {
              behavior.vm.handlePassedTimes();

              expect(behavior.vm.$data.localLabels.length).toEqual(0);
              expect(behavior.vm.$data.runningLabels.length).toEqual(1);

              expect({ ...behavior.vm.$data.runningLabels[0], playedIn: false, playedOut: false })
                .toEqual(sortedLabels[1]);

              behavior.setData({ localLabels: [...sortedLabels] });
              behavior.setProps({ musicalTime: 100 });
              behavior.vm.$nextTick(() => {
                behavior.vm.handlePassedTimes();

                expect(behavior.vm.$data.localLabels.length).toEqual(0);
                expect(behavior.vm.$data.runningLabels.length).toEqual(0);

                behavior.setData({ localLabels: [...sortedLabels] });
                behavior.setProps({ musicalTime: 0 });
                behavior.vm.$nextTick(() => {
                  behavior.vm.handlePassedTimes();

                  expect(behavior.vm.$data.localLabels.length).toEqual(3);
                  expect(behavior.vm.$data.runningLabels.length).toEqual(1);

                  expect({ ...behavior.vm.$data.localLabels[0], playedIn: false, playedOut: false })
                    .toEqual(sortedLabels[1]);
                  expect({ ...behavior.vm.$data.localLabels[1], playedIn: false, playedOut: false })
                    .toEqual(sortedLabels[2]);
                  expect({ ...behavior.vm.$data.localLabels[2], playedIn: false, playedOut: false })
                    .toEqual(sortedLabels[3]);
                  expect(
                    { ...behavior.vm.$data.runningLabels[0], playedIn: false, playedOut: false },
                  )
                    .toEqual(sortedLabels[0]);
                });
              });
            });
          });
        });
      });
    });
  });
});
