/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable guard-for-in */
/* eslint-disable no-restricted-syntax */
import Vue from 'vue';
import type { Wrapper } from '@vue/test-utils';
import { shallowMount } from '@vue/test-utils';
import type { VueClass } from 'vue-class-component/lib/declarations';
import type AnalysisActions from '@/logic/AnalysisActions';
import type {
  Label, LabelId, Pixel, Staff, Unit,
} from '@/data';
import { STAFF_ID_OFFSET } from '@/data';
import type { ImageSyncFormat } from '@/formats/UnitParser';
import UnitParser from '@/formats/UnitParser';
import StaffParser from '@/formats/StaffParser';
import type { CanvasEventSource, TestEmitter } from '@/components/canvas/canvas-events';

export const MOCK_LABEL_ID = 0;
export const MOCK_STAFF_ID = STAFF_ID_OFFSET;

export class TestAnalysisActions implements Record<keyof AnalysisActions, unknown> {
  labels: Record<LabelId, Label>;

  constructor(labels: Record<LabelId, Label> = {}) {
    this.labels = labels;
  }

  deleteId = jest.fn();

  updateLabel = jest.fn();

  createLabel = jest.fn(() => MOCK_LABEL_ID);

  select = jest.fn();

  getSelected = jest.fn(() => null);

  getPrettySelected = jest.fn(() => null);

  setTemplate = jest.fn();

  getTemplate = jest.fn(() => ({ tag: 'Pattern', type: 'Pattern' }));

  findFreeStaff = jest.fn(() => MOCK_STAFF_ID);

  getLabel = jest.fn((id) => this.labels[id] ?? null);

  getNext = jest.fn(() => null);

  getPrev = jest.fn(() => null);

  deleteSelected = jest.fn(() => null);

  getSortedLabels = jest.fn(() => Object.values(this.labels));

  updateTypesFilter = jest.fn(() => null);

  updateLayersFilter = jest.fn(() => null);

  getFilterValueFor = jest.fn(() => true);

  updateSearchString = jest.fn(() => null);

  selectedId = null;

  selectCount = 0;

  targetCount = 0;

  tagEditCount = 0;

  editCount = 0;

  template = null;

  labelFilters = null;
}

export function mockActions(labels: Record<LabelId, Label> = {}): TestAnalysisActions {
  return new TestAnalysisActions(labels) as unknown as TestAnalysisActions;
}

export function resetActions(actions: AnalysisActions): void {
  for (const key in actions) {
    const prop = (actions as any)[key];
    if (jest.isMockFunction(prop)) {
      prop.mockClear();
    }
  }
}

const positions: ImageSyncFormat = {
  'onset-x': [
    { onset: 0, x: 0 },
    { onset: 1, x: 10 },
    { onset: 2, x: 20 },
    { onset: 3, x: 30 },
    { onset: 4, x: 40 },
    { onset: 5, x: 50 },
    { onset: 6, x: 60 },
    { onset: 7, x: 70 },
    { onset: 8, x: 80 },
    { onset: 9, x: 90 },
    { onset: 10, x: 100 },
  ],
  bars: [],
  staffs: [
    { top: 0, bottom: 50 },
  ],
};

export function getFakePixels(): Unit<Pixel> {
  return UnitParser.createImageUnit(positions);
}

export function getFakeStaves(): Staff[] {
  return StaffParser.parseFormat(positions.staffs, 50 as Pixel);
}

export function getFakeStaff(): Staff {
  return getFakeStaves()[STAFF_ID_OFFSET];
}

export interface BehaviorContext {
  canvas: Vue,
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  behavior: Wrapper<Vue & Record<string, any>>
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  canvasEmit: jest.Mock<void, [string, ...any[]]>;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  selfEmit: jest.Mock<void, [string, ...any[]]>;
  testEmit: TestEmitter;
  actions: TestAnalysisActions;
  resetActions: () => void;
}

// eslint-disable-next-line @typescript-eslint/ban-types
export function initBehaviorContext(Behavior: VueClass<Vue>, props: object): BehaviorContext {
  const actions = mockActions();

  // Mock a component canvas event source
  const canvas = new Vue();

  // Fake the canvas emitter
  const canvasEmit = jest.fn();

  // Fake the behavior emitter
  const selfEmit = jest.fn();

  const canvasProp: CanvasEventSource = {
    on: canvas.$on.bind(canvas),
    off: canvas.$off.bind(canvas),
    emit: canvasEmit,
  };

  // Mount the behavior so it can subscribe to events
  const behavior = shallowMount(Behavior, {
    propsData: {
      actions,
      canvas: canvasProp,
      ...props,
    },
  });

  behavior.vm.$emit = selfEmit;

  // Allow test files to emit from the canvas easily
  const testEmit = canvas.$emit.bind(canvas) as unknown as TestEmitter;

  return {
    behavior,
    canvas,
    canvasEmit,
    selfEmit,
    testEmit,
    actions,
    resetActions: () => resetActions(actions),
  };
}

export function expectAction(fn: jest.Mock, calls:unknown[][]): void {
  expect(fn).toBeCalledTimes(calls.length);
  calls.forEach((args) => expect(fn).toHaveBeenCalledWith(...args));
  fn.mockClear();
}

export function expectActionOrdered(fn: jest.Mock, calls:unknown[][]): void {
  expect(fn).toBeCalledTimes(calls.length);
  calls.forEach((args, i) => expect(fn).toHaveBeenNthCalledWith(i + 1, ...args));
  fn.mockClear();
}

export default {};
