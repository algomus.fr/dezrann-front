import LabelBehavior from '@/components/behaviors/LabelBehavior.vue';
import type { CanvasEventSource } from '@/components/canvas/canvas-events';
import 'vuetify';
import {
  initBehaviorContext,
} from './utils';

describe('LabelBehavior.vue', () => {
  it('check that cursor follows musical Time', async () => {
    // Create fake canvas
    const canvas: CanvasEventSource = {
      on: jest.fn(),
      off: jest.fn(),
      emit: jest.fn(),
    };
    // Get fake behavior context
    const {
      behavior,
    } = initBehaviorContext(LabelBehavior, { canvas });

    // Expect to be renderless
    expect(behavior.isVisible()).toBe(false);
    // Expect listener array to be empty
    expect(behavior.vm.cleaner.listeners).toEqual([]);

    // fake a call to register
    behavior.vm.register('lol', 'test');
    // Expect listener array updated
    expect(behavior.vm.cleaner.listeners).toEqual([['lol', 'test']]);
    // Expect on was called
    expect(canvas.on).toHaveBeenNthCalledWith(1, 'lol', 'test');

    // Destroy the behavior
    behavior.destroy();
    expect(behavior.vm.cleaner.listeners).toEqual([]);
    expect(canvas.off).toHaveBeenNthCalledWith(1, 'lol', 'test');
  });
});
