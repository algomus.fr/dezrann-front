/* eslint-disable no-restricted-syntax */
import { basename, resolve } from 'path';
import AnalysisParser from '@/formats/AnalysisParser';
import { readFile } from 'fs/promises';
import { readdirSync, realpathSync, statSync } from 'fs';

/** Read and check a .dez file */
async function checkAnalysisFile(path: string) {
  const fileName = basename(path);
  const file = await readFile(path, { flag: 'r' });
  const text = file.toString('utf-8');
  const content = JSON.parse(text);

  AnalysisParser.parseFormat(content, fileName);
}

const explored: Set<string> = new Set();

interface Res {
  path: string;
  isFile: boolean;
  isDir: boolean;
}

function checkFile(path: string): Res | null {
  if (explored.has(path)) return null;
  explored.add(path);

  const stat = statSync(path);
  if (stat.isSymbolicLink()) return checkFile(realpathSync(path));
  const isFile = stat.isFile();
  const isDir = stat.isDirectory();

  return { path, isDir, isFile };
}

// https://stackoverflow.com/a/45130990/2657597
/**
 * Synchronously iterate on the directory path
 */
function* getFilesRecursively(dir: string): Iterable<string> {
  const entries = readdirSync(dir, { withFileTypes: true });
  for (const { name } of entries) {
    try {
      const res = checkFile(resolve(dir, name));

      if (res) {
        const { isFile, isDir, path } = res;

        if (isDir) {
          yield* getFilesRecursively(path);
        } else if (isFile) {
          yield path;
        }
      }
    } catch (e) {
      // eslint-disable-next-line no-console
      console.error(`Could not read ${resolve(dir, name)}`, e);
    }
  }
}

/**
 * Recursively try to parse all .dez files in ./analyses
 */
describe('Try to recursively parse the analyses folder', () => {
  // For all available files
  for (const f of getFilesRecursively('./tests/unit/files/analyses')) {
    // Check file extension
    if (f.match('\\.dez$')) {
      // Check as a unit test
      it(`Parsing '${f}': `, async () => checkAnalysisFile(f));
    }
  }
});
