import Vue from 'vue';
import Vuetify from 'vuetify';
import { mount } from '@vue/test-utils';
import '@testing-library/jest-dom';
import RefLinks from '@/components/navigation/RefLinks.vue';
import VueI18n from 'vue-i18n';

Vue.use(Vuetify);
Vue.use(VueI18n);

const vuetify = new Vuetify();

const i18n = new VueI18n({
  locale: 'fr',
});

describe('RefLinks.vue', () => {
  it('expects not to render anything if empty given refs are empty/wrong', async () => {
    const propsData = {
      refs: {},
    };
    const comp = mount(RefLinks, { i18n, vuetify, propsData });
    expect(comp.element).toBeEmptyDOMElement();
  });

  it('expects to render empty span when wrong refs are given', async () => {
    const propsData = {
      refs: { wikidia: 'The_Four_Seasons_(Vivaldi)' },
    };
    const comp = mount(RefLinks, { i18n, vuetify, propsData });
    expect(comp.element).toContainHTML('<span><!----></span>');
  });

  it('expects to be redirected to wikipedia', async () => {
    const propsData = {
      refs: { wikipedia: 'The_Four_Seasons_(Vivaldi)' },
    };
    const comp = mount(RefLinks, { i18n, vuetify, propsData });
    expect(comp.find('a').element.getAttribute('href')).toEqual('https://en.wikipedia.org/wiki/The_Four_Seasons_(Vivaldi)');
  });

  it('expects to render as much links as given refs', async () => {
    const propsData = {
      refs: {
        wikipedia: 'The_Four_Seasons_(Vivaldi)',
        imslp: 'Prelude_and_Fugue_in_C_minor,_BWV_847_(Bach,_Johann_Sebastian)',
        musicbrainz: '23c05cd3-0f72-3a61-9aa5-65a5be34010f',
      },
    };
    const comp = mount(RefLinks, { i18n, vuetify, propsData });
    const links = comp.findAll('a');
    expect(links).toHaveLength(3);
    expect(links.at(0).element.getAttribute('href')).toEqual('https://en.wikipedia.org/wiki/The_Four_Seasons_(Vivaldi)');
    expect(links.at(1).element.getAttribute('href')).toEqual('https://imslp.org/wiki/Prelude_and_Fugue_in_C_minor,_BWV_847_(Bach,_Johann_Sebastian)');
    expect(links.at(2).element.getAttribute('href')).toEqual('https://musicbrainz.org/work/23c05cd3-0f72-3a61-9aa5-65a5be34010f');
  });
});
