import Vue from 'vue';
import Vuetify from 'vuetify';
import VueI18n from 'vue-i18n';
import type { Wrapper } from '@vue/test-utils';
import { shallowMount } from '@vue/test-utils';
import ExportDialog from '@/components/dialogs/ExportDialog.vue';
import VuetifyDialog from 'vuetify-dialog';

Vue.use(Vuetify);
Vue.use(VueI18n);

const vuetify = new Vuetify();

const messages = {
  fr: {
    dialogs: {
      'export-analysis': {
        title: 'Exporter l\'analyse en local',
        info: {
          export: 'Cette analyse sera exporté sur votre ordinateur, vous pouvez choisir le nom du fichier qui correspondra à votre analyse.',
        },
        label: {
          analysis: 'Nom de l\'analyse',
        },
      },
    },
  },
};
const i18n = new VueI18n({
  locale: 'fr',
  messages,
});

const vuetifyDialogOptions = {
  context: { vuetify, i18n },
};

Vue.use(VuetifyDialog, vuetifyDialogOptions);

interface DialogVmType {
  vm :{
    name: string,
    export: () => string | boolean,
  }
}

type DialogType = Wrapper<ExportDialog> & DialogVmType;

// eslint-disable-next-line max-len
const factory = (filename: string) => shallowMount(ExportDialog, { i18n, attrs: { filename } }) as DialogType;

describe('ExportDialog.vue', () => {
  it('check the name format acceptance', () => {
    // Setting wrong names to check if it won't be accepted
    const dialog = factory('??');

    expect(dialog.vm.export()).toBe(false);

    dialog.vm.name = 'açça';

    expect(dialog.vm.export()).toBe(false);

    // Setting good names
    dialog.vm.name = 'test';

    expect(dialog.vm.export()).toBe('test.dez');
  });
});
