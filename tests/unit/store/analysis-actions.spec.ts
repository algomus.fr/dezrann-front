/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-var-requires */
import type AnalysisActions from '@/logic/AnalysisActions';
import AnalysisActionsImpl from '@/logic/AnalysisActionsImpl';
import type {
  Duration,
  Label,
  LabelId,
  Onset,
  Piece, Pixel, Source, Staff,
} from '@/data';
import {
  Analysis, Signature,
} from '@/data';
import type { AudioImageSyncFormat, AudioSyncFormat } from '@/formats/UnitParser';
import UnitParser from '@/formats/UnitParser';
import { SortedLabels } from '@/logic/SortedLabels';

// Mocking a piece, using data from /beethoven-violon

const audioSync: AudioSyncFormat = [
  {
    date: 0,
    onset: 0,
  },
  {
    date: 583.811,
    onset: 99,
  },
];
const audioImageSync: AudioImageSyncFormat = {
  'date-x': [
    {
      date: 0,
      x: 0,
    },
    {
      date: 583.811,
      x: 3003,
    },
  ],
  staffs: [
    {
      bottom: 355.0,
      top: 5.0,
    },
  ],
};

const secondUnit = UnitParser.createAudioUnit(audioSync);
const pixelUnit = UnitParser.createAudioImageUnit(secondUnit, audioImageSync);

const piece: Piece = {
  signature: new Signature({ numerator: 4, denominator: 4, upbeat: 0 }),
  sources: [{
    id: 0,
    pixelUnit,
    width: 3003 as Pixel,
    height: 360 as Pixel,
    staves: [{
      id: 0, top: 0, bottom: 564, height: 564, name: 'all', type: 2,
    }, {
      id: 1, top: 0, bottom: 34, height: 34, name: 'top.1', type: 1,
    }, {
      id: 2, top: 34, bottom: 68, height: 34, name: 'top.2', type: 1,
    }, {
      id: 3, top: 68, bottom: 102, height: 34, name: 'top.3', type: 1,
    }, {
      id: 4, top: 462, bottom: 496, height: 34, name: 'bot.1', type: 1,
    }, {
      id: 5, top: 496, bottom: 530, height: 34, name: 'bot.2', type: 1,
    }, {
      id: 6, top: 530, bottom: 564, height: 34, name: 'bot.3', type: 1,
    }, {
      id: 7, top: 107, bottom: 457, height: 350, name: '1', type: 0,
    }] as Staff[],
  }] as unknown as Source[],
} as Piece;

// The reference to the actions that will be tested
let actions: AnalysisActions;
// The reference to the SortedLabel inside actions
let sorted: SortedLabels;

function checkSorted() {
  const isSorted = sorted
    .labels
    .every((label, index, arr) => !index || SortedLabels.compare(label, arr[index - 1]));
  if (!isSorted) throw new Error('Array not sorted');
}

function createNLabel(n: number): void {
  for (let i = 0; i < n; i += 1) {
    actions.createLabel();
  }
}

// Utility functions meant to represent large amount of operations

function spreadLabels(): void {
  const labels = [...sorted.labels];

  labels.forEach((l, index) => actions.updateLabel(l.id, 'onset', index as Onset));
  checkSorted();
}

function reverseLabels(): void {
  const labels = [...sorted.labels];

  labels.forEach((l) => actions.updateLabel(l.id, 'onset', -l.onset as Onset));
  checkSorted();
}

function swapLabelOnsetDuration(): void {
  const labels = [...sorted.labels];

  labels.forEach((l) => {
    const { onset } = l;
    actions.updateLabel(l.id, 'onset', l.duration as unknown as Onset);
    actions.updateLabel(l.id, 'duration', Math.abs(onset) as Duration);
  });
  checkSorted();
}

function deleteHalfOfAllLabels(): void {
  const labels = [...sorted.labels];

  labels.forEach((l) => (l.id % 2) && actions.deleteId(l.id));
  checkSorted();
}

function deleteAllLabels(): void {
  const labels = [...sorted.labels];

  labels.forEach((l) => actions.deleteId(l.id));
  checkSorted();
}

/**
 * Describes the behavior of the AnalysisActions object
 */

describe('AnalysisActions, create, update, and export label', () => {
  beforeEach(() => {
    actions = new AnalysisActionsImpl(piece, new Analysis([]));
  });

  it('create, update, and export one label', () => {
    const id = actions.createLabel();

    actions.updateLabel(id, 'onset', 5 as Onset);
    actions.updateLabel(id, 'type', 'foo');
    actions.updateLabel(id, 'tag', 'bar');
    actions.updateLabel(id, 'comment', 'hello');

    actions.select(id);
    const label = actions.getSelected();
    expect(label?.onset).toEqual(5);
    expect(label?.type).toEqual('foo');
    expect(label?.tag).toEqual('bar');
    expect(actions.getPrettySelected()).toEqual('5 m2+1/4 0 foo bar # hello');
  });
});

describe('AnalysisActions, stress test for sorting', () => {
  beforeEach(() => {
    actions = new AnalysisActionsImpl(piece, new Analysis([]));
    sorted = (actions as any).sortedLabels;

    // Forbid actions from reseting due to bad internal state
    jest.spyOn(sorted, 'reset').mockImplementation(
      () => { throw new Error('reset was called during routine operations'); },
    );
  });

  it('simple create', () => {
    createNLabel(1000);
  });

  it('simple create then delete', () => {
    createNLabel(1000);
    deleteAllLabels();
  });

  it('create, modify', () => {
    createNLabel(1000);
    deleteAllLabels();
  });

  it('create, modify then delete', () => {
    createNLabel(1000);
    spreadLabels();
    deleteAllLabels();
  });

  it('stress test', () => {
    createNLabel(1000);
    spreadLabels();
    createNLabel(1000);
    reverseLabels();
    swapLabelOnsetDuration();
    spreadLabels();
    deleteHalfOfAllLabels();
    createNLabel(1000);
    swapLabelOnsetDuration();
    reverseLabels();
    createNLabel(10);
  });

  it('ultra stress test', () => {
    createNLabel(100);
    spreadLabels();
    deleteHalfOfAllLabels();
    swapLabelOnsetDuration();
    spreadLabels();
    swapLabelOnsetDuration();
    swapLabelOnsetDuration();
    reverseLabels();
    reverseLabels();
    createNLabel(200);
    deleteHalfOfAllLabels();
    createNLabel(100);
    deleteHalfOfAllLabels();
    createNLabel(100);
    spreadLabels();
    createNLabel(100);
    swapLabelOnsetDuration();
    createNLabel(100);
    reverseLabels();
    swapLabelOnsetDuration();
    spreadLabels();
    spreadLabels();
    swapLabelOnsetDuration();
    spreadLabels();
    swapLabelOnsetDuration();
    createNLabel(100);
    spreadLabels();
    reverseLabels();
    spreadLabels();
    createNLabel(100);
    reverseLabels();
    reverseLabels();
    spreadLabels();
    createNLabel(100);
    swapLabelOnsetDuration();
    createNLabel(100);
    spreadLabels();
    swapLabelOnsetDuration();
    spreadLabels();
    reverseLabels();
    spreadLabels();
    createNLabel(100);
    reverseLabels();
    reverseLabels();
    swapLabelOnsetDuration();
    reverseLabels();
    deleteHalfOfAllLabels();
    reverseLabels();
    swapLabelOnsetDuration();
    createNLabel(200);
    reverseLabels();
    spreadLabels();
    swapLabelOnsetDuration();
    reverseLabels();
    reverseLabels();
    swapLabelOnsetDuration();
    swapLabelOnsetDuration();
    spreadLabels();
    deleteHalfOfAllLabels();
    createNLabel(100);
    spreadLabels();
    spreadLabels();
    createNLabel(100);
    swapLabelOnsetDuration();
    createNLabel(100);
    deleteHalfOfAllLabels();
    createNLabel(100);
    swapLabelOnsetDuration();
    spreadLabels();
    createNLabel(100);
    deleteHalfOfAllLabels();
    swapLabelOnsetDuration();
    createNLabel(100);
    swapLabelOnsetDuration();
    deleteHalfOfAllLabels();
    reverseLabels();
    reverseLabels();
    reverseLabels();
    reverseLabels();
    swapLabelOnsetDuration();
    deleteHalfOfAllLabels();
    createNLabel(100);
    spreadLabels();
    deleteHalfOfAllLabels();
    reverseLabels();
    swapLabelOnsetDuration();
    reverseLabels();
    spreadLabels();
    spreadLabels();
  });

  afterEach(() => {
    const labelsFromSortedArray = [...sorted.labels];
    const labelMap = (actions as any).analysis.labels as Record<LabelId, Label>;
    const labelFromMap = Object.values(labelMap);

    labelsFromSortedArray.sort((a, b) => a.id - b.id);
    labelFromMap.sort((a, b) => a.id - b.id);

    expect(labelsFromSortedArray).toStrictEqual(labelFromMap);
  });
});
