/* eslint-disable @typescript-eslint/no-var-requires */
import type { AudioImageSyncFormat, AudioSyncFormat } from '@/formats/UnitParser';
import UnitFormat from '@/formats/UnitParser';
import type { Onset, Pixel, Second } from '@/data';

describe('Unit', () => {
  /**
   * Here are described the behavior of the onset-pixel conversion object
   * Previous version: https://gitlab.com/algomus.fr/dezrann/-/blob/d5e4a6058bc88440256b513d4bfe9d32fdbc43a6/code/essais/sc-client/test/test.js
   * Issue: https://gitlab.com/algomus.fr/dezrann/-/issues/342
   */

  // A fake onset - second conversion format
  const audioSyncFile: AudioSyncFormat = [
    { onset: 0.0, date: 0.0 },
    { onset: 1.0, date: 0.1 },
    { onset: 2.0, date: 0.2 },
    { onset: 3.0, date: 0.3 },
    { onset: 4.0, date: 0.4 },
    { onset: 5.0, date: 0.5 },
    { onset: 6.0, date: 0.6 },
    { onset: 7.0, date: 0.7 },
    { onset: 8.0, date: 0.8 },
    { onset: 9.0, date: 0.9 },
  ];
  // A fake second-pixel conversion format
  const dateToX = [
    { date: 0.1, x: 100 },
    { date: 0.2, x: 200 },
    { date: 0.3, x: 300 },
    { date: 0.4, x: 400 },
    { date: 0.5, x: 500 },
    { date: 0.6, x: 600 },
    { date: 0.7, x: 700 },
    { date: 0.8, x: 800 },
    { date: 0.9, x: 900 },
  ];

  const audioImageSyncFile: AudioImageSyncFormat = {
    'date-x': dateToX,
    staffs: [],
  };

  const secondUnit = UnitFormat.createAudioUnit(audioSyncFile);
  const pixelUnit = UnitFormat.createAudioImageUnit(secondUnit, audioImageSyncFile);

  describe('Partial apply combine to full apply', () => {
    it('should work toward onset', () => {
      dateToX.forEach(({ x }, index) => {
        expect(
          pixelUnit.fromSecondToOnset(pixelUnit.fromPixelToSecond(x as Pixel)).value,
        ).toEqual(
          audioSyncFile[index + 1].onset,
        );
      });
      expect(
        pixelUnit.fromSecondToOnset(pixelUnit.fromPixelToSecond(175 as Pixel)).value,
      ).toEqual(
        1.75,
      );
    });
    it('should work towards pixel', () => {
      audioSyncFile.slice(1).forEach(({ onset }, index) => {
        expect(
          pixelUnit.fromOnsetToSecondToPixel(onset as Onset),
        ).toEqual(
          dateToX[index].x,
        );
      });
      expect(
        pixelUnit.fromOnsetToSecondToPixel(1.75 as Onset),
      ).toEqual(
        175,
      );
    });
    it('should work second toward onset', () => {
      audioSyncFile.forEach(({ onset, date }) => {
        expect(pixelUnit.fromSecondToOnset(date as Second).value).toEqual(onset);
      });
    });
    it('should work onset toward second', () => {
      audioSyncFile.forEach(({ onset, date }) => {
        expect(pixelUnit.fromOnsetToSecond(onset as Onset)).toEqual(date);
      });
    });
    it('should work second toward pixel', () => {
      dateToX.forEach(({ date, x }) => {
        expect(pixelUnit.fromSecondToPixel(date as Second)).toEqual(x);
      });
    });
    it('should work pixel toward second', () => {
      dateToX.forEach(({ date, x }) => {
        expect(pixelUnit.fromPixelToSecond(x as Pixel)).toEqual(date);
      });
    });
    it('should work same as Unit<second> second toward onset', () => {
      [-10, 0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 100].forEach((date) => {
        expect(
          pixelUnit.fromSecondToOnset(date as Second).value,
        ).toEqual(
          secondUnit.toOnset(date as Second).value,
        );
      });
    });
    it('should work same as Unit<second> onset toward second', () => {
      [-10, 0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 100].forEach((onset) => {
        expect(
          pixelUnit.fromOnsetToSecond(onset as Onset),
        ).toEqual(
          secondUnit.fromOnset(onset as Onset).value,
        );
      });
    });
  });
  describe('2 points synchro', () => {
    it('from onset to pixel', () => {
      const unit = UnitFormat.createAudioImageUnit(
        UnitFormat.createAudioUnit([
          { onset: 0, date: 0 },
          { onset: 100, date: 100 },
        ]),
        {
          'date-x': [
            { date: 0, x: 0 },
            { date: 100, x: 100 },
          ],
          staffs: [],
        },
      );
      expect(unit.fromOnsetToSecondToPixel(12 as Onset)).toEqual(12);
      expect(unit.fromOnsetToSecondToPixel(0 as Onset)).toEqual(0);
      expect(unit.fromOnsetToSecondToPixel(100 as Onset)).toEqual(100);
    });
  });
});
