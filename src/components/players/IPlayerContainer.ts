import type { Onset, Second } from '@/data';

export default interface IPlayerContainer {
  setRealTime(value: Second): void;
  setMusicalTime(value: Onset): void;

  reset(): void;
}
