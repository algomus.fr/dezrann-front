// Due to the way vue is packaged, to use functions on vue components
// you need to declare the function types next to them

export default class PlayerContainer extends Vue {
  setTime(value: Onset): void;

  reset(): void;
}
