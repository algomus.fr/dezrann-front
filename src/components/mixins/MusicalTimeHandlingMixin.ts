import {
  Vue, Component, Prop, PropSync, Ref,
} from 'vue-property-decorator';
import type { Audio, Onset, Second } from '@/data';
import type IPlayer from '../players/IPlayer';

@Component
export default class MusicalTimeHandlingMixin extends Vue {
  @Ref('player')
  player?: IPlayer;

  @Prop({ required: true })
  audio!: Audio | null;

  @PropSync('realTime')
  realTimeSync!: Second | null;

  @PropSync('musicalTime')
  musicalTimeSync!: Onset | null;

  get currentTime(): Second | null {
    return this.realTimeSync;
  }

  set currentTime(time: Second | null) {
    if (!this.audio) return;
    this.realTimeSync = time;
    this.musicalTimeSync = time && this.audio.secondUnit.toOnset(time).value;
  }

  public setMusicalTime(onset: Onset): void {
    if (!this.audio) return;

    const current = this.audio.secondUnit.toOnset(this.realTimeSync || 0 as Second);

    const newOnsetSecond = this.audio.secondUnit.fromOnset(onset);

    const second = this.audio.secondUnit
      .fromOnset(onset, newOnsetSecond.repeat ? current.repeat : undefined).value;
    this.player?.setCurrentTime(second);
  }
}
