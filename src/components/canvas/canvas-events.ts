import type { Label, Point, Staff } from '@/data';

/**
 * All DOM keyboard events the canvas listens to
 */
type KeyboardEventName =
  'keydown' |
  'keyup';

/**
 * All DOM mouse events the canvas listens to
 */
type MouseEventName =
  'click' |
  'dblclick' |
  'mouseup' |
  'mousedown' |
  'mouseover' |
  'mousemove';

/**
 * All DOM wheel events the canvas listens to
 */
type WheelEventName =
  'wheel';

/**
 * All DOM focus events the canvas listens to
 *
 * 'focus' is not used as it doesn't bubble
 */
type FocusEventName =
  'focusin';

/**
 * All DOM events the canvas listens to
 *
 * They are not yet modified by the canvas (ctrl-eventname, etc.)
*/
export type BaseEventName = KeyboardEventName | MouseEventName | WheelEventName | FocusEventName;

/**
 * All events that cannot have a modifier, such as ctrl
 */
type UnmodifiableEvents =
  'keyup' |
  'mousemove' |
  'mouseup' |
  'focusin';

/**
 * All the events that can have a modifier, such as ctrl
 *
 * Basically all events except 'focus', 'up' and 'move' events
 */
export type ModifiableEventName = Exclude<BaseEventName, UnmodifiableEvents>;

/**
 * Transforms 'event' into 'event' | 'shift-event' | 'ctrl-event' | 'shift-ctrl-event' ...
 */
type GetModifiers<T extends string> =
  `${T}` |
  `alt-${T}` |
  `shift-${T}` |
  `shift-alt-${T}` |
  `ctrl-${T}` |
  `ctrl-alt-${T}` |
  `ctrl-shift-${T}` |
  `ctrl-shift-alt-${T}`;

/**
 * Transforms 'event' into 'event' | 'shift-event' | 'ctrl-event' | 'shift-ctrl-event'
 *
 * But only if it is modifiable;
 */
type WithModifiers<T extends string> =
  T extends ModifiableEventName ? GetModifiers<T> :
    T extends UnmodifiableEvents ? T :
      never;

/**
 * All possible events that can be listened to, from 'mousemove' to 'ctrl-shift-keydown'
 */
export type EventName = WithModifiers<BaseEventName>;

/**
 * The source tags for sources associated with a label
 */
export type LabelSourceTag =
  'label' |
  'right-arrow' |
  'left-arrow';

/**
 * The source tags for sources associated with a staff
 */
export type StaffSourceTag =
  'staff';

/**
 * The source tags for with no associated data
 */
export type RootSourceTag =
  'all';

/**
 * All possible sources of canvas events
 */
export type SourceTag = LabelSourceTag | StaffSourceTag | RootSourceTag;

/**
 * If that type of source should have a label in the payload
 */
export function canvasSourceIsLabel(source: SourceTag): source is LabelSourceTag {
  return source === 'label' || source === 'right-arrow' || source === 'left-arrow';
}

/**
 * If the DOM event name can be transformed into the '[ctrl-][shift-]event' format
 */
export function canvasEventIsModifiable(source: BaseEventName): source is ModifiableEventName {
  return source !== 'mouseup'
    && source !== 'mousemove'
    && source !== 'mouseover'
    && source !== 'keyup'
    && source !== 'focusin';
}

/**
 * The extended mouse event with the pixel position
 */
export interface CanvasMouseEvent extends Point {
  inner: MouseEvent,
}

/**
 * The extended wheel event with the pixel position
 */
export interface CanvasWheelEvent extends CanvasMouseEvent {
  inner: WheelEvent,
}

/**
 * Get the type of the event from the event name, for the canvas listeners
 */
type CanvasEventType<E extends EventName> =
  E extends MouseEventName ? CanvasMouseEvent :
    E extends WheelEventName ? CanvasWheelEvent :
      E extends KeyboardEventName ? KeyboardEvent :
        E extends FocusEventName ? FocusEvent :
          never;

/**
 * Get the arguments to an event from the event source (label, staff, ...)
 */
type Payload<S extends SourceTag> =
  S extends LabelSourceTag ? [Label, Staff] :
    S extends StaffSourceTag ? [Staff] :
      S extends RootSourceTag ? [] :
        never;

/**
* A function to react to a canvas event
*/
type CanvasListener<E extends EventName, S extends SourceTag> =
  (event: CanvasEventType<E>, ...payload: Payload<S>) => void;

/**
* A function to react to document events
*/
type DocumentListener<E extends EventName> =
  (event: CanvasEventType<E>) => void;

/**
* A template to register a canvas event listener
*/
type CanvasRegisterer<E extends EventName, S extends SourceTag> =
  (name: `${S}-${WithModifiers<E>}`, listener: CanvasListener<E, S>) => void;

/**
* A template to register a document event listener
*/
type DocumentRegisterer<E extends EventName> =
  (name: `document-${WithModifiers<E>}`, listener: DocumentListener<E>) => void;

/**
* All available signatures of the register canvas function
* All event names are associated with their event type and payload
* For example the 'label-click' handler has the signature (MouseEvent, Label, Staff) => void
*/
export type Registerer =
  // Add label events to the registerer
  CanvasRegisterer<MouseEventName, LabelSourceTag> &
  CanvasRegisterer<WheelEventName, LabelSourceTag> &
  CanvasRegisterer<KeyboardEventName, LabelSourceTag> &
  CanvasRegisterer<FocusEventName, LabelSourceTag> &
  // Add staff events to the registerer
  CanvasRegisterer<MouseEventName, StaffSourceTag> &
  CanvasRegisterer<WheelEventName, StaffSourceTag> &
  CanvasRegisterer<KeyboardEventName, StaffSourceTag> &
  CanvasRegisterer<FocusEventName, StaffSourceTag> &
  // Add root events to the registerer
  CanvasRegisterer<MouseEventName, RootSourceTag> &
  CanvasRegisterer<WheelEventName, RootSourceTag> &
  CanvasRegisterer<KeyboardEventName, RootSourceTag> &
  CanvasRegisterer<FocusEventName, RootSourceTag> &
  // Add document events to the registerer
  DocumentRegisterer<MouseEventName> &
  DocumentRegisterer<WheelEventName> &
  DocumentRegisterer<KeyboardEventName> &
  DocumentRegisterer<FocusEventName>;

/**
* A function to emit a canvas event
*/
type CanvasEmitter<E extends EventName, S extends SourceTag> =
(name:`${S}-${WithModifiers<E>}`, event: CanvasEventType<E>, ...payload: Payload<S>) => void;

/**
 * A function to emit a canvas event
 *
 * More permissive than CanvasEmitter
 *
 * Used in tests to allow partial events
 */
type CanvasTestEmitter<E extends EventName, S extends SourceTag> =
  (name:`${S}-${WithModifiers<E>}`, event: unknown, ...payload: Payload<S>) => Promise<void>;

/**
* A function to emit document events
 * More permissive than DocumentEmitter
 *
 * Used in tests to allow partial events
*/
type DocumentEmitter<E extends EventName> =
  (name:`document-${WithModifiers<E>}`, event: CanvasEventType<E>) => void;

type DocumentTestEmitter<E extends EventName> =
  (name:`document-${WithModifiers<E>}`, event: unknown) => Promise<void>;

/**
 * All available signature of the $emit canvas function
 *
 * Only use for tests, as behavior should not fake canvas events
 */
export type Emitter =
// Add label events to the emitter
CanvasEmitter<MouseEventName, LabelSourceTag> &
CanvasEmitter<WheelEventName, LabelSourceTag> &
CanvasEmitter<KeyboardEventName, LabelSourceTag> &
CanvasEmitter<FocusEventName, LabelSourceTag> &
// Add staff events to the emitter
CanvasEmitter<MouseEventName, StaffSourceTag> &
CanvasEmitter<WheelEventName, StaffSourceTag> &
CanvasEmitter<KeyboardEventName, StaffSourceTag> &
CanvasEmitter<FocusEventName, StaffSourceTag> &
// Add root events to the emitter
CanvasEmitter<MouseEventName, RootSourceTag> &
CanvasEmitter<WheelEventName, RootSourceTag> &
CanvasEmitter<KeyboardEventName, RootSourceTag> &
CanvasEmitter<FocusEventName, RootSourceTag> &
// Add document events to the emitter
DocumentEmitter<MouseEventName> &
DocumentEmitter<WheelEventName> &
DocumentEmitter<KeyboardEventName> &
DocumentEmitter<FocusEventName>;

/**
 * A more permissive variant of Emitter
 *
 * Allows tests to emit all canvas functions with less requirements
 */
export type TestEmitter =
  Emitter &
  // Add label events to the emitter
  CanvasTestEmitter<MouseEventName, LabelSourceTag> &
  CanvasTestEmitter<WheelEventName, LabelSourceTag> &
  CanvasTestEmitter<KeyboardEventName, LabelSourceTag> &
  CanvasTestEmitter<FocusEventName, LabelSourceTag> &
  // Add staff events to the emitter
  CanvasTestEmitter<MouseEventName, StaffSourceTag> &
  CanvasTestEmitter<WheelEventName, StaffSourceTag> &
  CanvasTestEmitter<KeyboardEventName, StaffSourceTag> &
  CanvasTestEmitter<FocusEventName, StaffSourceTag> &
  // Add root events to the emitter
  CanvasTestEmitter<MouseEventName, RootSourceTag> &
  CanvasTestEmitter<WheelEventName, RootSourceTag> &
  CanvasTestEmitter<KeyboardEventName, RootSourceTag> &
  CanvasTestEmitter<FocusEventName, RootSourceTag> &
  // Add document events to the emitter
  DocumentTestEmitter<MouseEventName> &
  DocumentTestEmitter<WheelEventName> &
  DocumentTestEmitter<KeyboardEventName> &
  DocumentTestEmitter<FocusEventName> &
  ((name: string, event: unknown, ...payload: unknown[]) => Promise<void>);

export interface CanvasEventSource {
  on: Registerer;
  off: Registerer;
  emit: Emitter;
}

export interface TestCanvasEventSource {
  on: Registerer;
  off: Registerer;
  emit: TestEmitter;
}
