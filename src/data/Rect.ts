import type { Pixel } from './Unit';

export interface Point {
  x: Pixel;
  y: Pixel;
}

export interface Rect extends Point {
  width: Pixel;
  height: Pixel;
}
