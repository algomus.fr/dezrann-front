export interface AnalysisStoreState {
  showRawTimes: boolean;
}

export const state = (): AnalysisStoreState => ({
  showRawTimes: false,
});
