import type { ActionTree } from 'vuex';
import type { AnalysisStoreState } from './analysis.state';

export const actions: ActionTree<AnalysisStoreState, AnalysisStoreState> = {
  toggleRawTimes({ commit }) {
    commit('toggleRawTimes');
  },
};
