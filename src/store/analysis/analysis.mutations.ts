import type { MutationTree } from 'vuex';
import type { AnalysisStoreState } from './analysis.state';

export const mutations: MutationTree<AnalysisStoreState> = {
  toggleRawTimes(state: AnalysisStoreState) {
    state.showRawTimes = !state.showRawTimes;
  },
};
