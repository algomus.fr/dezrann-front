# Format Classes

All the classes in this folder help parsing data from a JSON file format

They each define an interface for the data to parse, and define a static function to parse it

They all extend the target class, allowing them to access its protected constructor
