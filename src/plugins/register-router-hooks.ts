import Component from 'vue-class-component';

// Register the router hooks with their names\
// Allows us to use those router hooks inside the class definition
Component.registerHooks([
  'beforeRouteEnter',
  'beforeRouteLeave',
  'beforeRouteUpdate',
]);
