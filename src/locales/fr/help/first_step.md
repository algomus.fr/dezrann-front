- Sélectionnez une pièce dans la barre de
navigation à gauche. Recherchez la pièce qui vous intéresse par nom ou compositeur.
Une fois la pièce ouverte:
- Déplacez-vous horizontalement dans la pièce,
le long de la partition ou de la forme d'onde.
La carte, partition/forme d'onde réduite, fonctionne
comme une barre de défilement.
- Quand un fichier audio ou vidéo est disponible, jouez la musique
- Rajoutez des étiquettes avec un glissé-déplacé à la souris.
- Cliquez sur une étiquette pour la sélectionner.
L'<i>étiquette sélectionnée</i> est affichée avec des flèches noires sur les deux côtés.
- Modifiez l'étiquette sélectionnée, soit à la souris
(en la déplaçant ou bien avec les flèches noires),
soit dans l'onglet Modifier
dans la barre d'édition en bas.