Label buttons ease the creation of same or similar labels.
They are saved with your analysis.

<b>Create buttons.</b>
Select the <kbd>Create +</kbd> tab.
Click on the pencil icon to edit buttons.
Enter a new tag in the box, and add it using <kbd>Enter</kbd>.
As you type, suggestions will appear above the editor.

In the button editor:

- <kbd>Erase</kbd> removes the last button, or the selected button.

- <kbd>←</kbd> and <kbd>→</kbd> change the selected button.

- <kbd>,</kbd> adds the text entry to the button list.

- <kbd>Enter</kbd> adds the text entry and closes the editor.

<b>Use buttons.</b>
Click on any label button
(or use the keyboard shortcuts <kbd>1</kbd>/<kbd>2</kbd>/<kbd>3</kbd>/...),
then place a label on the score/waveform/spectrogram.
When audio and/or video are available, the buttons can also be used during playback.
