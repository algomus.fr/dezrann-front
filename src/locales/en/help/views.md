Pieces may contain several *views* on the music such as scores, waveforms or spectograms.

The top right button switches views.

The + button at the bottom right of the views opens a new view.
