Each *label* is defined by:
- A *start*, and possibly a duration and an end, in either musical time (number of bars) or wall clock time (seconds).
- A *type* (or category)
- An optional *tag* (short text, ideally in a controlled vocabulary).
- An optional *comment* (free text)
Labels can be edited either with the mouse on the score/waveform/spectograms, or, in fields in the Edit tab.
Labels are colored by their type, and sometimes by their tag.

An **analysis** is a set of labels.
In the navigation bar, you can:
- Save the analysis to the Dezrann cloud
- Save the analysis on your computer (.dez file)
Once saved, you can load the analysis at any time.