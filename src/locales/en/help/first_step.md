- Select a piece in the navigation bar on the left . Search pieces by title or by composer.

Once the piece is opened:
- Navigate the piece horizontally, along the score or the waveform. Below the score/waveform, the map works like a scrollbar.
- When an audio or video file is available, play the music.
- Add labels with a click and drag of the mouse.
- Click on a label to select it. The selected label appears with black arrows on both sides.
- Edit the selected label either with the mouse (move, or use the blacks arrows) or with the Edit tab at the bottom 