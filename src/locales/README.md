
# Translating / Localizing Dezrann

We are extremely grateful to all [translators](https://gitlab.com/algomus.fr/dezrann/dezrann/-/blob/dev/AUTHORS.md#contributors) of Dezrann who help to provide users worldwide with the best experience.

## Preliminaries

**Understand Dezrann.**
The *target audience* of Dezrann includes both scholars analyzing and/or browsing a piece of corpus, and children/students/people (either from the general public, or from music courses) hearing music and possibly discovering bits of music analysis.

**Do not work alone.** Talk with translators in other languages and developers to see what are the challenges in writing UI and corpora text.

**A full experience for the audience.** Translating/localizing Dezrann involves both translating the User Interface (UI) and translating the titles and descriptions of corpora pages.

## 1. Localizing Dezrann UI

**Special terms, be consistent.** Make sure that you use the same terminology, phrasing, and tone throughout the UI.  Begin by discussing the most important terms (some of them are in `elements.json:generic`, but they are used across many items.

| **en**  | **analysis** | **layer** | **label** | **type** (category) | **tag** (value) | **comment** |
| -       | -            | -         | -         | -        | -       | -           |
| **de**  | Analyse      | Schicht   | Label     | Typ      | Tag     | Kommentar   |
| **hr**  | analiza      | sloj      | oznaka?   | vrsta    | oznaka  | kommentar   |
| **el**  | Ανάλυση      | Επίπεδο   | Ετικέτα   | Τύπος    | ?       | Σχόλιο      |
| **fr**  | analyse      | couche    | étiquette | type     | tag     | commentaire |
| **sl**  | analize      | sloj      | etiketo   | vrsta    | oznaka  | kommentar   |
| **it**  | analisi      | strato    | etichetta | tipo     | tag     | commento    |

An *analysis* is a set of *labels*. 
Each *label* is defined:
- by a start, and possibly by a duration and an end;
- by a *type* (or *category*), such as "Pattern", "Harmony", "Cadence", "Structure", "Phrase"... This is from a controled vocabulary;
- by an optional *tag* (or *value*) (short text), indicating further details, depending on the type, such as "A", "B'", "Fm7", "ii6", "PAC", "I:PAC", "Verse 1". This should again be somewhat controlled according to the application;
- by an optional *comment* (free text), that can have several lines;
- by one or several optional *layers*, indicating the annotation source(s), such as
"tovey", "dcml", "algorithm-5".

The *label* word appears in many places in the application, it should be a very common word. (And please do not use the same term for *tag*, we kept in Greek/French/Italian 'tag', even if it's an English word).

**Keep the user in mind.** Remember that UI is all about the user. Make sure that your translations are designed to make the user's experience as smooth and intuitive as possible. Use language and formatting that is familiar and easy to understand, and consider the user's cultural background and expectations.

**Prioritize clarity.** Users should be able to quickly and easily understand the meaning of each element in the interface. Avoid using idiomatic expressions or overly complex vocabulary that could be confusing to non-native speakers.
When faced with character limits, prioritize clarity and essential meaning over stylistic flourishes or structural elements.

## 2. Localizing Dezrann corpora

To give a full experience to the user, we also translate/localize the corpora pages accessible from <https://www.dezrann.net/corpora>.

**Human expertise/curation is the key.** As of 2024, these texts are alltogether 1 of 2 pages of text. To initiate your translations, you may use automated translating services such as Large Langage Models However, sometimes, automated translations completely misinterpret the meaning. Anyway, you can either start from these automated translations, then correct them, or create translations from scratch based on the English version `corpora.json`.

**Special terms.** Please pay special attention to technical details/terms in music (such as classical style, sonata form, harmony, etc.). For several corpora, we also mention that the data is "free/open" ("libre"), meaning it is not just free of charge ("gratis"). Please select the most relevant term for your language.

**The different strings.**
- `shorttitle` and `motto` are for <https://www.dezrann.net/corpora>. Please keep the `shorttitle` very short (< 30 characters). Please ensure the coherency of the entire page, please refer to the English page.
- `title`, `text` and `availability` are for the individual corpus pages, such as <https://www.dezrann.net/explore/openscore-lieder>. Please check and update various links, particularly Wikipedia pages, to include the most relevant ones for your language.


## How to

The following instructions are if you are a git user. If you are not confortable with git, you can send a mail with the files to someone in the team.

You can directly work with the files in this directory and/or use a tool such as the [i18n Ally extension for VS Code](https://github.com/lokalise/i18n-ally).


**Step 1, UI**, on this git
- Decide the main terms and fill the table above
- Start translating by copying the `en/` directory to a new directory with your locale. Note that you can have *empty* or *missing* keys in some files.
- Translate the strings on the right of each line. If you're unsure, leave them empty `""`, we can address them later, but *don't leave untranslated English string*.
- (i18n Ally: is there a way to empty all the strings at once?)
- Push a branch and create a MR with your files.
- The Dezrann team will contact you for additional details if needed

**Step 2, Corpora pages**, on the git dezrann-corpus
- Translate the *corpora pages* (`corpus/*/corpus.json` in the git [dezrann-corpus](https://gitlab.com/algomus.fr/dezrann/dezrann-corpus/-/tree/main/corpus)), that is the keys `title`, `shorttitle`, `motto`, `text`, `availability`.
- Push a branch and create a MR with your files.
- The Dezrann team will contact you for additional details if needed

**Step 3** (later, once a first version is online)
- Check remaining strings
- Translate the *intro/help pages* (TODO: rework them in English first)
- Test your translation with at least one other native speaker

## Further resources

- https://developers.google.com/style/translation
