import type { Label, LabelId } from '@/data';

/**
 * Actions shared by both AnalysisActions and SynchronizationActions
 *
 * Allows some behavior to work for both AnalysisEditor and SynchronizationEditor
 */
export default interface SharedActions {
  /** Delete an element of the canvas */
  deleteId(id: LabelId): void;
  /** Select an element of the canvas, optionally ask for focus from the main canvas */
  select(id: LabelId | null, requestTarget?: boolean): void;
  /** Returns the canvas selection if it exists */
  getSelected(): Label | null;
  /** Exports the canvas selection if it exists */
  getPrettySelected(): string | null ;
  /** Returns the canvas selection if it exists */
  getLabel(id: LabelId): Label | null;
  /** Select the next element of the canvas, possibly using a predicate to iterate on them */
  getNext(id: LabelId | null, predicate?: (e: Label) => boolean): Label | null;
  /** Select the previous element of the canvas, possibly using a predicate to iterate on them */
  getPrev(id: LabelId | null, predicate?: (e: Label) => boolean): Label | null;
  /** Delete selected element from the canvas, and  possibly try to select the next one */
  deleteSelected(selectNext?: boolean, requestTarget?: boolean): Label | null;

  readonly selectedId: LabelId | null;
  readonly editCount: number;
  readonly selectCount: number;
  readonly targetCount: number;
}
