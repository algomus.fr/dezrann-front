export interface CssGenerator {
  renderCss(): string;

  /**
   * A marker to increase on update
   * Changing it will cause the css to re-render
   */
  modificationCounter: number;
}

/**
   * Transform a string into a valid css identifier
   * Collisions are possible if special characters are used
   */
export function makeCssIdentifier(value: string): string {
  // https://stackoverflow.com/questions/10619126/make-sure-string-is-a-valid-css-id-name
  // We must be careful not to replace into an invalid string,
  // thus adding 'a' in some cases and doing a second replace.
  return value?.replace(/(^-\d-|^\d|^-\d|^--)/, 'a$1').replace(/[\W]/g, '-') || '';
}
