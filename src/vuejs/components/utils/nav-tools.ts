import { Client } from '@/client';
import type { Corpus } from '@/client/corpus';
import { Piece } from '@/data';
import type { Route } from 'vue-router';

/**
 * Transforms the value of $route.query[key] into string | null
 * This is done by only taking the first element in case of an array
 */
export function extractQuery(q: string | null | (string | null)[]): string | null {
  const notArray = Array.isArray(q) ? q[0] : q;
  return notArray ?? null;
}

/**
 * Transforms the value of $route.query[key] into an int number | null
 * This is done by only taking the first element in case of an array
 */
export function extractQueryInt(q: string | null | (string | null)[]): number | null {
  const num = Number.parseInt(extractQuery(q) as string, 10);
  return Number.isFinite(num) ? num : null;
}

/**
 * Transforms the value of $route.query[key] into string | null
 * This is done by only taking the first element in case of an array
 */
export function getPiecePath(route: Route): string {
  if (!route.params.piecePath) throw new Error('piecePath is not defined');
  return route.params.piecePath;
}

/**
 * Transforms the value of $route.query[key] into string | null
 * This is done by only taking the first element in case of an array
 */
export function getClient(route: Route): Client {
  if (route.meta?.client instanceof Client) return route.meta?.client;
  throw new Error('client is not defined');
}

/**
 * Remove the .dez from an analysis file name, if present
 * @param name an analysis id, ie. 'test.dez'
 * @returns the shortened id, ie. 'test
 */
export function trimAnalysisName(name: string): string {
  if (name.slice(name.length - 4) !== '.dez') return name;
  return name.slice(0, -4);
}

/**
 * Allows getting a link using either a Piece object or just params
 */
interface PieceLike {
  path: string;
  isLocal: boolean;
}

/**
 * Returns the link to a loaded piece
 */
export function getPieceLink(piece: PieceLike): string {
  return (piece.isLocal ? '/~~/' : '/~/') + piece.path;
}

/**
 * Returns the link to the sync editor of a loaded piece
 */
export function getPieceSyncLink(piece: PieceLike): string {
  return (piece.isLocal ? '/local-sync/' : '/sync/') + piece.path;
}

/**
 * Returns the index of a given piece in a given list a pieces,
 * relative to its id.
 */
export function getIndexPieceAlt(piece: Piece, list: Piece[]): number {
  return list.indexOf(list.filter((el) => el.id === piece.id)[0]);
}

/**
 * Returns the given path without the last part,
 * relative to the given char.
 */
export function removeLast(path: string, char: string): string {
  const list = path.split(char);
  const bin = list.pop();
  if (bin === '') list.pop();

  return list.join('/');
}

export function sortCorpus(corpus: Piece[]): Piece[] {
  return corpus.sort((a: Piece, b: Piece) => Piece.compareOpus(a, b));
}

/**
 * Returns all pieces in the same corpus as the given piece,
 * related to the given corpora.
 */
export async function getCorpus(piece: Piece, corpora: Corpus): Promise<Piece[]> {
  const { path: piecePath } = piece;
  const corpusPath = removeLast(piecePath, '/');
  const corpus = corpora.pieces.filter(({ path }) => ((removeLast(path, '/') === corpusPath) && (path.split('/').pop() !== piecePath.split('/').pop())));
  corpus.push(piece);

  return corpus;
}
